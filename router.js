const express = require('express');
const router = express.Router();
const User = require('./controllers/user-controller');
const Content = require('./controllers/content-controller');

router.get('/user', User.show);

router.get('/content', Content.show);

module.exports = router;