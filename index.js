const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000
const router = require('./router');
require('./models');

app.use(express.json());
app.use('/', router);
app.get('/',(req,res) => {
    res.status(200).json({
        status: "Success",
        message: "hello world"
    })
})

app.listen(PORT,() => {
    console.log(`Listening on ${PORT}`)
})