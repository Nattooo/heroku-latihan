class Content {
    static show(req, res) {
        res.status(201).json({
            status: "success",
            message: "Content page"
        })
    }
}

module.exports = Content;